import sys


def print_check(k, n, check):
    """
    Функия изменяющая k.

    Функия изменяет k или останавливает программу, в зависимости от команды пользователя,
    если она не верна то k не будет изменен и выведется 'Er'.
    Команда << - переместит на 1 страницу.
    Команда < - переместит на 1 страницу влево.
    Команда >> - переместит на последнюю страницу.
    Команда > - переместит на 1 страницу вправо.
    Команда 'stop' - остановка программы.
    Если будет введено число, то произойдет перемещение на страницу с таким номером, если она существует.

    Args:
        n:  Количество страниц в пагинации.
        k:  Номер страницы, на которой мы находимся.
        check: Команда пользователя.

    Return:
        Возвращает изменненное k при правильности команды и неравности ее 'stop', в последнем случае вернет 'stop',
        но если команда неверна вернет 'Er'.
    """
    commands = {'<<': lambda k, n: 1, '<': lambda k, n: k - 1, '>>': lambda k, n: n, '>': lambda k, n: k + 1}
    if check == 'stop':
        return 'stop'
    for c in commands:
        if check == c:
            k = commands[c](k, n)
            if k == 0:
                k = n
            elif k == n + 1:
                k = 1
            return k
    else:
        if check.isdigit() and 0 < int(check) <= n:
            k = int(check)
            return k
        else:
            print('Я вас не понимаю!')
            print('stop - остановка программы')
            return 'Er'


def test_print_check_1():
    """
    Проверяет правильность поведения функции print_check при вводе команды 'stop' с помощью юнит-тестов.
    """
    check = 'stop'
    k = 0
    n = 0
    assert print_check(k, n, check) == check


def test_print_check_2():
    """
    Проверяет правильность поведения функции print_check при вводе команды '<<' с помощью юнит-тестов.
    """
    check = '<<'
    k = 10
    n = 10
    assert print_check(k, n, check) == 1


def test_print_check_3():
    """
    Проверяет правильность поведения функции print_check при вводе команды '<' с помощью юнит-тестов.
    """
    check = '<'
    k = 0
    n = 10
    assert print_check(k, n, check) == n


def test_print_check_4():
    """
    Проверяет правильность поведения функции print_check при вводе команды '>' с помощью юнит-тестов.
    """
    check = '>'
    k = 10
    n = 10
    assert print_check(k, n, check) == 1


def test_print_check_5():
    """
    Проверяет правильность поведения функции print_check при вводе команды '>>' с помощью юнит-тестов.
    """
    check = '>>'
    k = 10
    n = 10
    assert print_check(k, n, check) == n


def test_print_check_6():
    """
    Проверяет правильность поведения функции print_check при вводе команды числа с помощью юнит-тестов.
    """
    check = '5'
    k = 10
    n = 10
    assert print_check(k, n, check) == int(check)


def test_print_check_7():
    """
    Проверяет правильность поведения функции print_check при вводе команды неверной команды с помощью юнит-тестов.
    """
    check = 'Er'
    k = 10
    n = 10
    assert print_check(k, n, check) == check


def if_str(what, do, or_do):
    """
    Является инструкцией if в одну строчку.

    Выполняет do при истинности выражения what, и or_do при ложности выражения.
    Заменяет инструкцию if, для выполнения основного блока кода в 1 строчку.

    Args:
        what:   Аргумент, принимающий логическое выражение.
        do:     Аргумент, возвращающийся если what == True.
        or_do:  Аргумент, возвращающийся, если what == False.

    Returns:
        Выводит do или or_do, в зависимости от равенства what.
    """
    if what:
        return do
    else:
        return or_do


def test_if_str_1():
    """
    Проверяет правильность поведения функции if_str при входе в нее Истины с помощью юнит-тестов.
    """
    assert if_str(5 < 4, 5, 4) == 4


def test_if_str_2():
    """
    Проверяет правильность поведения функции if_str при входе в нее Лжи с помощью юнит-тестов.
    """
    assert if_str(5 > 4, 5, 4) == 5


def in_check(n, k, m):
    """
    Проверяет правильность введенных аргументов.

    Проверяет являются ли все запрашиваемые у пользователя аргументы целыми числами, а так же их
    соответствие некоторым условиям: n > 1, m > 1, k >= 1,  k < n,  m < (n // 2).

    Args:
        n:  Количество страниц в пагинации.
        k:  Номер страницы, на которой мы находимся.
        m:  Количество страниц выводимых помимо той, на которой мы находимся слева(справа так же).

    Returns:
        Возвращает строку 'stop', останавливающую программу в дальнейшем при несоответствии хотябы одному из условий,
        или None, в обратной ситуации, а так же все аргументы в таком-же порядке.
    """
    try:
        n, k, m = int(n), int(k), int(m)
        if n <= 1 or m <= 1 or k < 1 or k > n or m > (n // 2):
            print('Вы ввели переменные неправильно!')
            return 'stop', n, k, m
        else:
            return None, n, k, m
    except ValueError:
        print('Вы ввели переменные неправильно!')
        return 'stop', n, k, m


def test_in_check_1():
    """
    Проверяет правильность поведения функции in_check при верных значениях с помощью юнит-тестов.
    """
    n = 10
    m = 4
    k = 2
    check, n1, k1, m1 = in_check(n, k, m)
    assert check == None and n1 == n and k1 == k and m1 == m


def test_in_check_2():
    """
    Проверяет правильность поведения функции in_check при неверном m с помощью юнит-тестов.
    """
    n = 10
    m = 10
    k = 10
    check, n1, k1, m1 = in_check(n, k, m)
    assert check == 'stop' and n1 == n and k1 == k and m1 == m


def test_in_check_3():
    """
    Проверяет правильность поведения функции in_check при неверном k с помощью юнит-тестов.
    """
    n = 10
    m = 10
    k = 'LKGMA'
    check, n1, k1, m1 = in_check(n, k, m)
    assert check == 'stop' and n1 == n and k1 == k and m1 == m


def string_creator(k, n, m):
    """
    Возвращает строку пагинации.

    Создает строку, в которую добавляет <<, при не равенстве k единице, иначе пробел, даллее добавляет все цифры от k-m,
    если они больше 0, до k не включительно, после чего добавляет k в скобках, далее все цифры от k до m+k,
    если они меньше n, и >>, если k неравен n, иначе пробел, после чего возвращает эту строку.

    Args:
        n:  Количество страниц в пагинации.
        k:  Номер страницы, на которой мы находимся.
        m:  Количество страниц выводимых помимо той, на которой мы находимся слева(справа так же).

    Returns:
        Строка пагинации.
    """
    string = str(if_str(k > 1, '<<', '') + ' '.join([str(k - c) for c in range(m, 0, -1) if k - c >= 1])
                 + f' ({k}) ' + ' '.join([str(k + c) for c in range(1, m + 1) if k + c <= n]) + if_str(k < n, '>>', ''))
    return string


def test_string_creator():
    """
    Проверяет правильность поведения функции string_creator с помощью юнит-тестов.
    """
    k = 3
    m = 2
    n = 5
    assert string_creator(k, m, n) == '<<1 2 (3) 4 5>>'


def printer(check, n, k, m):
    """
    Выводит пагинацию в зависимости от желаний пользователя.

    Функия выводит пагинацию, или останавливает программу, в зависимости от значения check, по заданным
    параметрам n, k и m, после чего запрашивает у пользователя команду и снова выводит пагинацию,
    по изменненным параметрам если команда верна, иначе программа будет спрашивать у пользователя команды до
    того момента, пока не будет введена корректная.

    Args:
        check: переменная, определяющая будет ли выполнятся функия.
        n:  Количество страниц в пагинации.
        k:  Номер страницы, на которой мы находимся.
        m:  Количество страниц выводимых помимо той, на которой мы находимся слева(справа так же).

    Returns:
        Возвращает None.
    """
    while check != 'stop':
        string = string_creator(k, n, m)
        print(string)
        check = input('Чего изволите?')
        check = print_check(k, n, check)
        if check != 'stop':
            k = check


def main():
    n, k, m = input('Введите значения через пробел').split()
    check, n, k, m = in_check(n, k, m)
    printer(check, n, k, m)


if __name__ == '__main__':
    main()
